﻿
namespace PdfCustomization.Models
{
    public class QuarterlyCommitment
    {
        public string Quarter { get; set; }
        public int QuantityOfSugar { get; set; }
    }
}